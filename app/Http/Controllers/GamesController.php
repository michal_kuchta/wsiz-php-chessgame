<?php

namespace App\Http\Controllers;

use App\Events\NewRoomCreated;
use App\GameRoom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Str;

class GamesController extends Controller
{
    public function postGetRooms()
    {
        return response()->json([
            'success' => true,
            'results' => GameRoom::query()->get()->map(function (GameRoom $gameRoom){
                return [
                    'id' => $gameRoom->id,
                    'name' => $gameRoom->name
                ];
            })
        ]);
    }

    public function postCreateRoom()
    {
        $room = new GameRoom();
        $room->name = auth()->user()->getName();
        $room->uid = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 16);
        $room->creator_id = auth()->user()->id;
        $room->save();

        event(new NewRoomCreated($room->id, $room->name));

        return response()->json(['success' => true, 'url' => route('games.enterGame', [$room->uid])]);
    }

    public function postJoinRoom()
    {
        //GameRoomNotAvailable
    }

    public function getEnterGame($uid)
    {
        $room = GameRoom::getRoomByUid($uid);

        if (!$room)
            return redirect()->route('home');

        return view('game.room');
    }
}
