<?php

namespace App\Http\Controllers;

use App\Events\NewMessage;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function postSendMessage()
    {
        $channel = request()->input('channel', 'main-chat');
        $message = request()->input('message');
        $username = auth()->user()->login;
        $date = now()->format('Y-m-d H:i:s');
        $userId = auth()->user()->id;

        event(new NewMessage($channel, $message, $username, $userId, $date));

        return response()->json();
    }
}
