<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameRoom extends Model
{
    protected $table = 'game_rooms';
    protected $fillable = [
        'id',
        'name',
        'uid',
        'creator_id',
        'opponent_id'
    ];

    protected static function boot()
    {
        parent::boot();
    }

    public function creator()
    {
        return $this->hasOne(User::class, 'id', 'creator_id');
    }

    public function opponent()
    {
        return $this->hasOne(User::class, 'id', 'opponent_id');
    }

    public static function getRoomByUid($uid)
    {
        return self::query()->where('uid', $uid)->first();
    }
}
