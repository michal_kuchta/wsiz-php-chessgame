@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <main-room
                channel="main-chat"
                header="{{ __('Lobby') }}"
                join-button-text="{{ __('Dołącz') }}"
                name-header="{{ __('Nazwa') }}"
                add-room-button-text="{{ __('Utwórz pokój') }}"
                action-header="{{ __('Akcje') }}"
                join-room-url="{{ route('games.join') }}"
                create-room-url="{{ route('games.create') }}"
                rooms-list-url="{{ route('games.list') }}"
            ></main-room>
        </div>
        <div class="col-md-4">
            <main-chat
                channel="main-chat"
                header="{{ __('Chat') }}"
                message-placeholder="{{ __('Wiadomość') }}"
                button-text="{{ __('Wyślij') }}"
                send-url="{{ route('chat.sendMessage') }}"
                user-id="{{ auth()->user()->id }}"
                no-messages-text="{{ __('Brak wiadomości') }}"
            ></main-chat>
        </div>
    </div>
</div>
@endsection
