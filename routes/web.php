<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function(){
    Route::get('/', function () {
        return view('home');
    })->name('home');

    Route::post('chat/messages', "ChatController@postSendMessage")->name('chat.sendMessage');

    Route::post('games/join', "GamesController@postJoinRoom")->name('games.join');
    Route::post('games/create', "GamesController@postCreateRoom")->name('games.create');
    Route::post('games/list', "GamesController@postGetRooms")->name('games.list');
    Route::get('game/{uid}', "GamesController@getEnterGame")->name('games.enterGame');

    Route::post('logout', 'Auth\LoginController@logout')->name('logout');
});


Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');

Route::post('register', 'Auth\RegisterController@register')->name('register');

